CC=owcc

OBJECTS=sedt.o edit.o key.o buf.o vdu.o term.o com.o coma.o comb.o comc.o comd.o ent.o file.o text.o text1.o vars.o regexp.o regsub.o unix.o

# Compile with termcap
CFLAGS=-O -DUNIX -DTERMCAP -DANSIC -DLINUX -I/opt/owroot/include
LIBES=-L/opt/owroot/lib -llibtermcap

.EXTENSIONS:
.EXTENSIONS: .o .c

.c.o:
	$(CC) -c $(CFLAGS) -o $@ $< 
    
sedt: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) $(LIBES) -o sedt

all: sedt

clean: .SYMBOLIC
	rm -f *.o sedt
