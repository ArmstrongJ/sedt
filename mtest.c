#include <stdio.h>
#include <sys/types.h>
#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/MainW.h>
#include <Xm/ScrollBar.h>

main(argc,argv)
unsigned int argc;
char **argv;
{Display *display;
 Widget app_shell;
 Widget main;
 Widget hscroll;
 Widget vscroll;
 int ac;
 Arg al[10];
 
 XtToolkitInitialize();
 if (!(display=XtOpenDisplay(NULL,NULL,argv[0],"Sedt",NULL,0,&argc,argv)))
 {XtWarning("Sedt: can't open display");
  exit(0);
 }
 app_shell=XtAppCreateShell(argv[0],"Sedt",applicationShellWidgetClass,
  display,NULL,0);
 main=XmCreateMainWindow(app_shell,"main",NULL,0);
 XtManageChild(main);
 ac=0;
 XtSetArg(al[ac],XmNorientation,XmHORIZONTAL);
 ac++;
 XtSetArg(al[ac],XmCMinimum,1);
 ac++;
 XtSetArg(al[ac],XmCMaximum,100);
 ac++;
 XtSetArg(al[ac],XmCValue,1);
 ac++;
 hscroll=XmCreateScrollBar(main,"hscroll",al,ac);
 XtManageChild(hscroll);
 ac=0;
 XtSetArg(al[ac],XmCMinimum,1);
 ac++;
 XtSetArg(al[ac],XmCMaximum,100);
 ac++;
 XtSetArg(al[ac],XmCValue,1);
 ac++;
 vscroll=XmCreateScrollBar(main,"vscroll",NULL,0);
 XtManageChild(vscroll);
 XtRealizeWidget(app_shell);
 XtMainLoop();
}