#include <stdio.h>
#if defined(DOS) | defined(NT)
#include <malloc.h>
#endif

struct Item {struct Item *Next, *Last; unsigned char *Value;}
 *Oldlist=NULL, *Newlist=NULL, *Olditem, *Newitem, *T_Olditem, *T_Newitem;

char *Oldfile, *Newfile, *Listfile, Old_Buffer[513], List_Buffer[513];

int Binary=0, Oldline, Newline, T_Oldline, T_Newline, I, Inserted, Deleted,
 Howfar, Done, List_Line;

FILE *Old, *New, *List;

main(Argc,Argv)
int Argc;
char *Argv[];
{char *P, C;
 int I;
 Oldfile=Newfile=NULL;
 Get_Arg(Argc,Argv);
 Newline=Oldline=1;
 while (fgets(List_Buffer,512,List)!=NULL)
 {I=strlen(List_Buffer);
  if (I==0||List_Buffer[I-1]!='\n')
  {List_Buffer[I]='\n';
   List_Buffer[I+1]=0;
  }
  P=List_Buffer;
  if ((C=(*P++))=='d')
  {List_Line=(*P++)-'0';
   while ((C=(*P++))!=':')
    List_Line=List_Line*10+C-'0';
   while (Oldline!=List_Line)
   {if (fgets(Old_Buffer,512,Old)==NULL)
     Fatalerror("Not enough lines in old file");
    I=strlen(Old_Buffer);
    if (I==0||Old_Buffer[I-1]!='\n')
    {Old_Buffer[I]='\n';
     Old_Buffer[I+1]=0;
    }
    Oldline++;
    fputs(Old_Buffer,New);
    Newline++;
   }
   if (fgets(Old_Buffer,512,Old)==NULL||strcmp(P,Old_Buffer)!=0)
    Fatalerror("Old file does not match differences file");
   I=strlen(Old_Buffer);
   if (I==0||Old_Buffer[I-1]!='\n')
   {Old_Buffer[I]='\n';
    Old_Buffer[I+1]=0;
   }
   Oldline++;
  } else
  if (C=='i')
  {List_Line=(*P++)-'0';
   while ((C=(*P++))!=':')
    List_Line=List_Line*10+C-'0';
   while (Newline!=List_Line)
   {if (fgets(Old_Buffer,512,Old)==NULL)
     Fatalerror("Not enough records in old file");
    I=strlen(Old_Buffer);
    if (I==0||Old_Buffer[I-1]!='\n')
    {Old_Buffer[I]='\n';
     Old_Buffer[I+1]=0;
    }
    Oldline++;
    fputs(Old_Buffer,New);
    Newline++;
   }
   fputs(P,New);
   Newline++;
  } else
   Fatalerror("Illegal record in differences file");
 }
 while (fgets(Old_Buffer,512,Old)!=NULL)
  fputs(Old_Buffer,New);
 fclose(Old);
 fclose(New);
 fclose(List);
}

Get_Arg(Argc,Argv)
int Argc;
char *Argv[];
{char *P;
 for (I=1;I<Argc;I++)
 {if ((P=Argv[I])[0]=='-')
  {if (P[1]=='b'||P[1]=='B'&&P[2]==0)
    Binary=1; else
   if (P[1]=='a'||P[1]=='A'&&P[2]==0)
    Binary=0;
  } else
  if (Oldfile==NULL)
   Oldfile=Argv[I]; else
  if (Newfile==NULL)
   Newfile=Argv[I]; else
  if (Listfile==NULL)
   Listfile=Argv[I]; else
   Fatalerror("Too many file arguments");
 }
 if (Oldfile==NULL)
 {puts("");
  puts("Dif:  Create a differences file from an old and a new file");
  puts("Dif {-<N>} <Old file> <New file> {<Differences file>}");
  puts("");
  puts("Merg: Create a new file from an old file and a differences file");
  puts("Merg <Old file> <New file> {<Differences file>}");
  puts("");
  puts("Clist: Create a file containing unchanged, deleted and inserted lines");
  puts("Clist {-<N>} <Old File> <New file> {<Listing file>}");
  puts("");
  puts("N is the number of lines that must be the same for a match (default 2)");
#if defined(VMS)
  exit();
#endif
#if defined(DOS) | defined(NT)
  _exit(0);
#endif
 }
 if (Listfile==NULL)
  Listfile="CON";
 if ((Old=fopen(Oldfile,"r"))==NULL)
  Fatalerror("Could not find old file");
#if defined(VMS)
 if ((New=fopen(Newfile,"w","fop=tef","rat=cr","rfm=var"))==NULL)
#endif
#if defined(DOS) | defined(NT)
 if ((New=fopen(Newfile,"w"))==NULL)
#endif
  Fatalerror("Could not create new file");
 if ((List=fopen(Listfile,"r"))==NULL)
  Fatalerror("Could not find differences file");
}

Fatalerror(S)
char *S;
{printf(S);
 _exit(1);
}

#if defined(VMS)
memcpy(D,S,C)
char *D, *S;
int C;
{while (C--)
 (*D++)=(*S++);
}
#endif
