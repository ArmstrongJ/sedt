Sedt - A screen editor

You're looking at the sources for Sedt, a spectacular text editor
from years past designed to operate a bit like DEC's EDT.  The
original author, Anker Berg-Sonne, long ago released the sources
for this product after it's being developed for years as a 
shareware product.  This source code contains some modifications
to Sedt to keep the editor running on current platforms.