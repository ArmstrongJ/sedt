OBJECTS=sedt.o edit.o key.o buf.o vdu.o term.o com.o coma.o comb.o comc.o comd.o ent.o file.o text.o text1.o vars.o regexp.o regsub.o unix.o

# Compile with termcap
#CFLAGS=-O -DUNIX -DTERMCAP -DANSIC -DLINUX
#LIBES=-ltermcap

# Compile with curses
CFLAGS=-O0 -g -DUNIX -DANSIC -DLINUX
LIBES=-lcurses

#CC=owcc
CC=gcc


sedt:$(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) $(LIBES) -o sedt

all:sedt

clean:
	rm -f *.o sedt
